#!/usr/bin/python -tt

from module_items import *
import argparse

parser = argparse.ArgumentParser(description="Dump item ids with names, for use with the administrator items tool.")
parser.add_argument("output_file", nargs='?', default="admin_item_ids", help="file name without extension")
args = parser.parse_args()

def getstring(name, num):
	dupa = ""
	#print "name: {0}, num1: {1:02x}, num2: {2:02x}, res: {3:02x}".format(name, (num & 0xFF), (itp_type_arrows & 0xFF), (num & 0xFF) == (itp_type_arrows & 0xFF))
	if num & 0xFF == itp_type_horse & 0xFF:
		dupa += "horse"
	if num & 0xFF == itp_type_one_handed_wpn & 0xFF:
		dupa += "1hand"
	if num & 0xFF == itp_type_two_handed_wpn & 0xFF:
		dupa += "2hand"
	if num & 0xFF == itp_type_polearm & 0xFF:
		dupa += "polearm"
	if num & 0xFF == itp_type_arrows & 0xFF:
		dupa += "arrows"
	if num & 0xFF == itp_type_bolts & 0xFF:
		dupa += "bolts"
	if num & 0xFF == itp_type_shield & 0xFF:
		dupa += "shield"
	if num & 0xFF == itp_type_bow & 0xFF:
		dupa += "bow"
	if num & 0xFF == itp_type_crossbow & 0xFF:
		dupa += "crossbow"
	if num & 0xFF == itp_type_thrown & 0xFF:
		dupa += "thrown"
	if num & 0xFF == itp_type_goods & 0xFF:
		dupa += "goods"
	if num & 0xFF == itp_type_head_armor & 0xFF:
		dupa += "headarmor"
	if num & 0xFF == itp_type_body_armor & 0xFF:
		dupa += "bodyarmor"
	if num & 0xFF == itp_type_foot_armor & 0xFF:
		dupa += "footarmor"
	if num & 0xFF == itp_type_hand_armor & 0xFF:
		dupa += "handarmor"
	if num & 0xFF == itp_type_pistol & 0xFF:
		dupa += "pistol"
	if num & 0xFF == itp_type_musket & 0xFF:
		dupa += "musket"
	if num & 0xFF == itp_type_bullets & 0xFF:
		dupa += "bullets"
	if num & 0xFF == itp_type_animal & 0xFF:
		dupa += "animal"
	if num & 0xFF == itp_type_animal & 0xFF:
		dupa += "bool"
	return dupa

d = 0
with open(args.output_file + ".txt", "w") as f:
  for i, item in enumerate(items):
    #if (item[1].startswith("INVALID"): # or "Banner" in item[1]):
    #  continue
    if (item[0] == "all_items_end"):
      break
    #d = d + 1
    #if d > 10:
    #	break
    f.write("({0}, '{1}', '{2}'),\n".format(i, item[1].replace("'", ""), getstring(item[1], item[3])))




# itp_type_horse           = 0x0000000000000001
# itp_type_one_handed_wpn  = 0x0000000000000002
# itp_type_two_handed_wpn  = 0x0000000000000003
# itp_type_polearm         = 0x0000000000000004
# itp_type_arrows          = 0x0000000000000005
# itp_type_bolts           = 0x0000000000000006
# itp_type_shield          = 0x0000000000000007
# itp_type_bow             = 0x0000000000000008
# itp_type_crossbow        = 0x0000000000000009
# itp_type_thrown          = 0x000000000000000a
# itp_type_goods           = 0x000000000000000b
# itp_type_head_armor      = 0x000000000000000c
# itp_type_body_armor      = 0x000000000000000d
# itp_type_foot_armor      = 0x000000000000000e
# itp_type_hand_armor      = 0x000000000000000f
# itp_type_pistol          = 0x0000000000000010
# itp_type_musket          = 0x0000000000000011
# itp_type_bullets         = 0x0000000000000012
# itp_type_animal          = 0x0000000000000013
# itp_type_book            = 0x0000000000000014